from src.processor import Processor
from src.preprocess import Generator
from src.dataset import NTUFeeder, Graph
from torch.utils.data import DataLoader


if __name__ == '__main__':
    Generator().gendata()
    Processor().train()

    # graph = Graph()
    # feeder = NTUFeeder(data_shape=[3,6,300,25,2], connect_joint=graph.connect_joint)
    # train_loader = DataLoader(feeder, batch_size=4)
    # print(feeder.x_train.shape)
    # for x, y in train_loader:
    #     print(x.shape, y.shape)
