import pickle as pkl
import torch
import numpy as np
from torch.optim import SGD
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data import DataLoader
from . import dataset
from . import model


class CosineScheduler():
    def __init__(self, num_sample, max_epoch, warm_up):
        warm_up_num = warm_up * num_sample
        max_num = max_epoch * num_sample
        self.eval_interval = lambda epoch: 1 if (epoch+1) > max_epoch - 10 else 5
        self.lr_lambda = lambda num: num / warm_up_num if num < warm_up_num else 0.5 * (np.cos((num - warm_up_num) / (max_num - warm_up_num) * np.pi) + 1)

    def get_lambda(self):
        return self.eval_interval, self.lr_lambda


class Processor:
    def __init__(self):
        np.random.seed(1)
        torch.manual_seed(1)
        torch.cuda.manual_seed(1)
        torch.backends.cudnn.benchmark = True
        torch.backends.cudnn.enabled = True

        self.output_device = 0
        self.device =  torch.device('cuda:{}'.format(self.output_device))
        torch.cuda.set_device(self.output_device)

        self.train_batch_size = 4
        self.eval_batch_size = 2
        self.feeders, self.data_shape, self.num_class, self.A, self.parts = dataset.load()
        self.train_loader = DataLoader(self.feeders['train'],
            batch_size=self.train_batch_size, 
            # num_workers=4,
            pin_memory=True, shuffle=True, drop_last=True
        )
        # self.eval_loader = DataLoader(self.feeders['eval'],
        #     batch_size=self.eval_batch_size, num_workers=4,
        #     pin_memory=True, shuffle=False, drop_last=False
        # )
        self.model = model.ResGCN(
            data_shape=self.data_shape, 
            num_class=self.num_class, 
            A=torch.Tensor(self.A),
            structure=[1,2,3,3],
            parts=[torch.Tensor(part).long() for part in self.parts],
        ).to(self.device)
        self.model = torch.nn.DataParallel(self.model, device_ids=[0], output_device=self.output_device)
        self.optimizer = SGD(self.model.parameters(), **{
                'lr': 0.1, 
                'momentum': 0.9, 
                'nesterov': True, 
                'weight_decay': 0.0002
        })
        self.max_epoch = 10
        lr_scheduler = CosineScheduler(
            max_epoch=2, 
            warm_up=1, 
            num_sample=len(self.train_loader)
        )
        self.eval_interval, lr_lambda = lr_scheduler.get_lambda()
        self.scheduler = LambdaLR(self.optimizer, lr_lambda=lr_lambda)
        self.loss_func = torch.nn.CrossEntropyLoss().to(self.device)

    def train(self):
        for epoch in range(self.max_epoch):
            self.model.train()
            num_top1, num_sample = 0, 0
            for x, y in self.train_loader:
                self.optimizer.zero_grad()
                x = x.float().to(self.device)
                y = y.long().to(self.device)
                out, _ = self.model(x)
                loss = self.loss_func(out, y)
                loss.backward()
                self.optimizer.step()
                self.scheduler.step()
                num_sample += x.size(0)
                reco_top1 = out.max(1)[1]
                num_top1 += reco_top1.eq(y).sum().item()
                lr = self.optimizer.param_groups[0]['lr']
            train_acc = num_top1 / num_sample
            print('Epoch: {}/{}, Acc: {:.3f}, Loss: {:.3f}, Rate: {:.3f}'.format(epoch+1, self.max_epoch, train_acc, loss.item(), lr))
        with open('output/model.pkl', 'wb') as out:
            pkl.dump(self.model.state_dict(), out) 
