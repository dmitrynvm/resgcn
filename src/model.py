import torch
from torch import nn


def get_corr_joints(parts):
    num_joints = max([max(part) for part in parts]) + 1
    res = []
    for i in range(num_joints):
        for j in range(len(parts)):
            if i in parts[j]:
                res.append(j)
                break
    return torch.Tensor(res).long()


def init_param(modules):
    for m in modules:
        if isinstance(m, nn.Conv1d) or isinstance(m, nn.Conv2d):
            nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.BatchNorm2d):
            nn.init.constant_(m.weight, 1)
            nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.Linear):
            nn.init.normal_(m.weight, std=0.001)
            if m.bias is not None:
                nn.init.constant_(m.bias, 0)


def zero_init_last(modules):
    for m in modules:
        if isinstance(m, ResGCNModule):
            if hasattr(m.scn, 'bn_up'):
                nn.init.constant_(m.scn.bn_up.weight, 0)
            if hasattr(m.tcn, 'bn_up'):
                nn.init.constant_(m.tcn.bn_up.weight, 0)


class SpatialGraphConv(nn.Module):
    def __init__(self, in_channels, out_channels, max_graph_distance):
        super(SpatialGraphConv, self).__init__()
        self.s_kernel_size = max_graph_distance + 1
        self.gcn = nn.Conv2d(in_channels, out_channels*self.s_kernel_size, 1)

    def forward(self, x, A):
        x = self.gcn(x)
        n, kc, t, v = x.size()
        x = x.view(n, self.s_kernel_size, kc//self.s_kernel_size, t, v)
        x = torch.einsum('nkctv,kvw->nctw', (x, A[:self.s_kernel_size])).contiguous()
        return x


class PartAtt(nn.Module):
    def __init__(self, channel, parts):
        super(PartAtt, self).__init__()
        self.parts = parts
        self.joints = get_corr_joints(parts)
        inter_channel = channel // 4
        self.fcn = nn.Sequential(
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(channel, inter_channel, kernel_size=1),
            nn.BatchNorm2d(inter_channel),
            nn.ReLU(inplace=True),
            nn.Conv2d(inter_channel, channel*len(self.parts), kernel_size=1),
        )
        self.softmax = nn.Softmax(dim=-1)
        self.bn = nn.BatchNorm2d(channel)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        N, C, T, V = x.size()
        res = x
        x_att = self.softmax(self.fcn(x).view(N, C, len(self.parts)))
        x_att = torch.split(x_att, 1, dim=-1)
        x_att = [x_att[self.joints[i]].expand_as(x[:,:,:,i]) for i in range(V)]
        x_att = torch.stack(x_att, dim=-1)
        return self.relu(self.bn(x * x_att) + res)


class SpatialBasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, max_graph_distance, residual=False):
        super(SpatialBasicBlock, self).__init__()
        if not residual:
            self.residual = lambda x: 0
        elif in_channels == out_channels:
            self.residual = lambda x: x
        else:
            self.residual = nn.Sequential(nn.Conv2d(in_channels, out_channels, 1), nn.BatchNorm2d(out_channels))
        self.conv = SpatialGraphConv(in_channels, out_channels, max_graph_distance)
        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x, A):
        res_block = self.residual(x)
        x = self.conv(x, A)
        x = self.bn(x)
        x = self.relu(x + res_block)
        return x


class TemporalBasicBlock(nn.Module):
    def __init__(self, channels, temporal_window_size, stride=1, residual=False):
        super(TemporalBasicBlock, self).__init__()
        padding = ((temporal_window_size - 1) // 2, 0)
        if not residual:
            self.residual = lambda x: 0
        elif stride == 1:
            self.residual = lambda x: x
        else:
            self.residual = nn.Sequential(nn.Conv2d(channels, channels, 1, (stride,1)), nn.BatchNorm2d(channels))
        self.conv = nn.Conv2d(channels, channels, (temporal_window_size,1), (stride,1), padding)
        self.bn = nn.BatchNorm2d(channels)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x, res_module):
        res_block = self.residual(x)
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x + res_block + res_module)
        return x


class ResGCNModule(nn.Module):
    def __init__(self, in_channels, out_channels, A, initial=False, stride=1, kernel_size=[9,2]):
        super(ResGCNModule, self).__init__()
        if not len(kernel_size) == 2:
            raise ValueError()
        if not kernel_size[0] % 2 == 1:
            raise ValueError()
        temporal_window_size, max_graph_distance = kernel_size
        if initial:
            module_res, block_res = False, False
        module_res, block_res = True, False
        if not module_res:
            self.residual = lambda x: 0
        elif stride == 1 and in_channels == out_channels:
            self.residual = lambda x: x
        else:
            self.residual = nn.Sequential(nn.Conv2d(in_channels, out_channels, 1, (stride,1)), nn.BatchNorm2d(out_channels))
        self.scn = SpatialBasicBlock(in_channels, out_channels, max_graph_distance, block_res)
        self.tcn = TemporalBasicBlock(out_channels, temporal_window_size, stride, block_res)
        self.edge = nn.Parameter(torch.ones_like(A))

    def forward(self, x, A):
        return self.tcn(self.scn(x, A*self.edge), self.residual(x))


class AttGCNModule(nn.Module):
    def __init__(self, in_channels, out_channels, A, parts, stride=1, kernel_size=[9,2]):
        super(AttGCNModule, self).__init__()
        if not len(kernel_size) == 2:
            raise ValueError()
        if not kernel_size[0] % 2 == 1:
            raise ValueError()
        temporal_window_size, max_graph_distance = kernel_size
        module_res, block_res = True, False
        if not module_res:
            self.residual = lambda x: 0
        elif stride == 1 and in_channels == out_channels:
            self.residual = lambda x: x
        else:
            self.residual = nn.Sequential(
                nn.Conv2d(in_channels, out_channels, 1, (stride,1)),
                nn.BatchNorm2d(out_channels),
            )
        self.scn = SpatialBasicBlock(in_channels, out_channels, max_graph_distance, block_res)
        self.tcn = TemporalBasicBlock(out_channels, temporal_window_size, stride, block_res)
        self.att = PartAtt(out_channels, parts)
        self.edge = nn.Parameter(torch.ones_like(A))

    def forward(self, x, A):
        return self.att(self.tcn(self.scn(x, A*self.edge), self.residual(x)))


class ResGCNInputBranch(nn.Module):
    def __init__(self, structure, num_channel, A):
        super(ResGCNInputBranch, self).__init__()
        self.register_buffer('A', A)
        module_list = [ResGCNModule(num_channel, 64, A, initial=True)]
        module_list += [ResGCNModule(64, 64, A, initial=True) for _ in range(structure[0] - 1)]
        module_list += [ResGCNModule(64, 64, A) for _ in range(structure[1] - 1)]
        module_list += [ResGCNModule(64, 32, A)]
        self.bn = nn.BatchNorm2d(num_channel)
        self.layers = nn.ModuleList(module_list)

    def forward(self, x):
        N, C, T, V, M = x.size()
        x = self.bn(x.permute(0,4,1,2,3).contiguous().view(N*M, C, T, V))
        for layer in self.layers:
            x = layer(x, self.A)
        return x


class ResGCN(nn.Module):
    def __init__(self, structure, data_shape, num_class, A, parts):
        super(ResGCN, self).__init__()
        num_input, num_channel, _, _, _ = data_shape
        self.register_buffer('A', A)
        self.input_branches = nn.ModuleList([
            ResGCNInputBranch(structure, num_channel, A)
            for _ in range(num_input)
        ])
        module_list = [AttGCNModule(32*num_input, 128, A, parts, stride=2)]
        module_list += [AttGCNModule(128, 128, A, parts) for _ in range(structure[2] - 1)]
        module_list += [AttGCNModule(128, 256, A, parts, stride=2)]
        module_list += [AttGCNModule(256, 256, A, parts) for _ in range(structure[3] - 1)]
        self.main_stream = nn.ModuleList(module_list)
        self.global_pooling = nn.AdaptiveAvgPool2d(1)
        self.fcn = nn.Linear(256, num_class)
        init_param(self.modules())
        zero_init_last(self.modules())

    def forward(self, x):
        N, I, C, T, V, M = x.size()
        x_cat = []
        for i, branch in enumerate(self.input_branches):
            x_cat.append(branch(x[:,i,:,:,:,:]))
        x = torch.cat(x_cat, dim=1)
        for layer in self.main_stream:
            x = layer(x, self.A)
        _, C, T, V = x.size()
        feature = x.view(N, M, C, T, V).permute(0, 2, 3, 4, 1)
        x = self.global_pooling(x)
        x = x.view(N, M, -1).mean(dim=1)
        x = self.fcn(x)
        return x, feature
