import numpy as np
from torch.utils.data import Dataset


class NTUFeeder(Dataset):
    def __init__(self, data_shape, connect_joint):
        _, _, self.T, self.V, self.M = data_shape
        self.conn = connect_joint
        # data = np.load('output/data.npz')
        # self.x_train, self.y_train = data['x_train'], data['y_train']
        self.data = np.load('input/ntu25.npz')
        self.x_train = self.data['x_train']
        self.y_train = self.data['y_train']
        self.x_test = self.data['x_test']
        self.y_test = self.data['y_test']
        self.x_train = self.x_train.reshape((self.x_train.shape[0], self.x_train.shape[1], 3, 25, 1)).transpose(0, 2, 1, 3, 4)
        self.x_test = self.x_test.reshape((self.x_test.shape[0], self.x_test.shape[1], 3, 25, 1)).transpose(0, 2, 1, 3, 4)

    def __len__(self):
        return len(self.y_train)

    def __getitem__(self, idx):
        # (C, T, V, M) -> (I, C*2, T, V, M)
        sample = self.x_train[idx, :, :, :, :]
        C, T, V, M = sample.shape
        sample_ = np.zeros((3, C*2, T, V, M))
        sample_[0,:C,:,:,:] = sample
        for i in range(V):
            sample_[0,C:,:,i,:] = sample[:,:,i,:] - sample[:,:,1,:]
        for i in range(T-2):
            sample_[1,:C,i,:,:] = sample[:,i+1,:,:] - sample[:,i,:,:]
            sample_[1,C:,i,:,:] = sample[:,i+2,:,:] - sample[:,i,:,:]
        for i in range(len(self.conn)):
            sample_[2,:C,:,i,:] = sample[:,:,i,:] - sample[:,:,self.conn[i],:]
        bone_length = 0
        for i in range(C):
            bone_length += np.power(sample_[2,i,:,:,:], 2)
        bone_length = np.sqrt(bone_length) + 0.0001
        for i in range(C):
            sample_[2,C+i,:,:,:] = np.arccos(sample_[2,i,:,:,:] / bone_length)
        return sample_, self.y_train[idx]


def load():
    graph = Graph()
    feeders = {
        'train': NTUFeeder(data_shape=[3,6,300,25,2], connect_joint=graph.connect_joint)
    }
    return feeders, [3,6,120,25,2], 54, graph.A, graph.parts


class Graph():
    def __init__(self, max_hop=3, dilation=1):
        self.max_hop = max_hop
        self.dilation = dilation
        self.num_node, self.edge, self.connect_joint, self.parts = self._get_edge()
        self.A = self._get_adjacency()

    def __str__(self):
        return self.A

    def _get_edge(self):
        num_node = 25
        neighbor_1base = [
            (1, 2), 
            (2, 21), 
            (3, 21), 
            (4, 3), 
            (5, 21),
            (6, 5), 
            (7, 6), 
            (8, 7), 
            (9, 21), 
            (10, 9),
            (11, 10), 
            (12, 11), 
            (13, 1), 
            (14, 13), 
            (15, 14),
            (16, 15), 
            (17, 1), 
            (18, 17), 
            (19, 18), 
            (20, 19),
            (22, 23), 
            (23, 8), 
            (24, 25), 
            (25, 12)
        ]
        neighbor_link = [(i - 1, j - 1) for (i, j) in neighbor_1base]
        connect_joint = np.array([2,2,21,3,21,5,6,7,21,9,10,11,1,13,14,15,1,17,18,19,2,23,8,25,12]) - 1
        parts = [
            np.array([5, 6, 7, 8, 22, 23]) - 1,     # left_arm
            np.array([9, 10, 11, 12, 24, 25]) - 1,  # right_arm
            np.array([13, 14, 15, 16]) - 1,         # left_leg
            np.array([17, 18, 19, 20]) - 1,         # right_leg
            np.array([1, 2, 3, 4, 21]) - 1          # torso
        ]

        self_link = [(i, i) for i in range(num_node)]
        edge = self_link + neighbor_link
        return num_node, edge, connect_joint, parts

    def _get_hop_distance(self):
        A = np.zeros((self.num_node, self.num_node))
        for i, j in self.edge:
            A[j, i] = 1
            A[i, j] = 1
        hop_dis = np.zeros((self.num_node, self.num_node)) + np.inf
        transfer_mat = [np.linalg.matrix_power(A, d) for d in range(self.max_hop + 1)]
        arrive_mat = (np.stack(transfer_mat) > 0)
        for d in range(self.max_hop, -1, -1):
            hop_dis[arrive_mat[d]] = d
        return hop_dis

    def _get_adjacency(self):
        hop_dis = self._get_hop_distance()
        valid_hop = range(0, self.max_hop + 1, self.dilation)
        adjacency = np.zeros((self.num_node, self.num_node))
        for hop in valid_hop:
            adjacency[hop_dis == hop] = 1
        normalize_adjacency = self._normalize_digraph(adjacency)
        A = np.zeros((len(valid_hop), self.num_node, self.num_node))
        for i, hop in enumerate(valid_hop):
            A[i][hop_dis == hop] = normalize_adjacency[hop_dis == hop]
        return A

    def _normalize_digraph(self, A):
        Dl = np.sum(A, 0)
        num_node = A.shape[0]
        Dn = np.zeros((num_node, num_node))
        for i in range(num_node):
            if Dl[i] > 0:
                Dn[i, i] = Dl[i]**(-1)
        AD = np.dot(A, Dn)
        return AD
