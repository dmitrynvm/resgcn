import os 
import numpy as np


class Generator():
    def __init__(self):
        self.num_person_out = 1
        self.num_person_in = 4
        self.num_joint = 25
        self.max_frame = 300
        self.dataset = 'ntu'
        os.makedirs('output', exist_ok=True)
        self.file_list = []
        for filename in os.listdir('data'):
            self.file_list.append(('data', filename))

    def read_skeleton_filter(self, file):
        with open(file, 'r') as f:
            skeleton_sequence = {}
            skeleton_sequence['numFrame'] = int(f.readline())
            skeleton_sequence['frameInfo'] = []
            for t in range(skeleton_sequence['numFrame']):
                frame_info = {}
                frame_info['numBody'] = int(f.readline())
                frame_info['bodyInfo'] = []
                for m in range(frame_info['numBody']):
                    body_info = {}
                    body_info_key = [
                        'bodyID', 'clipedEdges', 'handLeftConfidence',
                        'handLeftState', 'handRightConfidence', 'handRightState',
                        'isResticted', 'leanX', 'leanY', 'trackingState'
                    ]
                    body_info = {
                        k: float(v)
                        for k, v in zip(body_info_key, f.readline().split())
                    }
                    body_info['numJoint'] = int(f.readline())
                    body_info['jointInfo'] = []
                    for v in range(body_info['numJoint']):
                        joint_info_key = [
                            'x', 'y', 'z', 'depthX', 'depthY', 'colorX', 'colorY',
                            'orientationW', 'orientationX', 'orientationY',
                            'orientationZ', 'trackingState'
                        ]
                        joint_info = {
                            k: float(v)
                            for k, v in zip(joint_info_key, f.readline().split())
                        }
                        body_info['jointInfo'].append(joint_info)
                    frame_info['bodyInfo'].append(body_info)
                skeleton_sequence['frameInfo'].append(frame_info)
        return skeleton_sequence

    def get_nonzero_std(self, s):  # (T,V,C)
        index = s.sum(-1).sum(-1) != 0  # select valid frames
        s = s[index]
        if len(s) != 0:
            s = s[:, :, 0].std() + s[:, :, 1].std() + s[:, :, 2].std()  # three channels
        else:
            s = 0
        return s

    def read_xyz(self, file):
        seq_info = self.read_skeleton_filter(file)
        data = np.zeros((self.num_person_in, seq_info['numFrame'], self.num_joint, 3))
        for n, f in enumerate(seq_info['frameInfo']):
            for m, b in enumerate(f['bodyInfo']):
                for j, v in enumerate(b['jointInfo']):
                    if m < self.num_person_in and j < self.num_joint:
                        data[m, n, j, :] = [v['x'], v['y'], v['z']]
        energy = np.array([self.get_nonzero_std(x) for x in data])
        index = energy.argsort()[::-1][0:self.num_person_out]
        data = data[index]
        data = data.transpose(3, 1, 2, 0)  # to (C,T,V,M)
        return data

    def gendata(self):
        labels = []
        paths = []
        for folder, filename in sorted(self.file_list):
            path = os.path.join(folder, filename)
            action_loc = filename.find('A')
            action_class = int(filename[(action_loc+1):(action_loc+4)])
            paths.append(path)
            labels.append(action_class - 1)
        
        # Create data tensor (N,C,T,V,M)
        samples = np.zeros((len(labels), 3, self.max_frame, self.num_joint, self.num_person_out), dtype=np.float32)
        # Fill (C,T,V,M) to data tensor (N,C,T,V,M)
        for i, s in enumerate(paths):
            data = self.read_xyz(s)
            samples[i, :, 0:data.shape[1], :, :] = data

        x_train = samples[:500, :, :, :, :]
        y_train = labels[:500]
        x_test = samples[500:, :, :, :, :]
        y_test = labels[500:]
        np.savez(
            'output/data.npz', 
            x_train=x_train,
            y_train=y_train,
            x_test=x_test,
            y_test=y_test
        )
